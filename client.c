#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

void receive_messages(int socket)
{
    char * buffer = (char *) calloc(384,sizeof(char));
    int bytes;
    while ((bytes = recv(socket,buffer,384,0)) != 0)
    {
        printf("%s",buffer);
        
        memset(buffer,0,384);
    }
    printf("Disconnected from server\n");
    close(socket);
    free(buffer);
    exit(1);
}

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        printf("Usage: client <target ip or hostname> <port>\n");
        printf("\n  Example: client 127.0.0.1 12345\n");
        return 1;
    }

    struct addrinfo hints, *res;
    int sockfd;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    
    getaddrinfo(argv[1],argv[2], &hints, &res);
    
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    
    if (connect(sockfd, res->ai_addr, res->ai_addrlen) == -1)
    {
        printf("ERROR!\n");
        printf("%s\n",strerror(errno));
        return 1;
    }
    
    int pid = fork();
    if (pid == 0)
    {
        receive_messages(sockfd);
        close(sockfd);
        return 0;
    }
    
    printf("Enter desired username: ");
    char* username = (char*) calloc(128,sizeof(char));
    fgets(username,127,stdin);
    send(sockfd, username, 128, 0);

    char* buffer = (char*) calloc(256,sizeof(char));
    int bytes, i;
    while (1)
    {
        fgets(buffer,256,stdin);
        for (i = strlen(buffer)+2; i != 0; i--)
        {
        //    fputc('\b',stdout);
        }
        
        if (!strcmp(buffer,"/close\n"))
        {
            close(sockfd);
            break;
        }
        bytes = send(sockfd, buffer, 256, 0);
        memset(buffer,0,256);
    }
    kill(pid, SIGTERM);
    close(sockfd);
    free(buffer);
    printf("Quit\n");
    
    return 0;
}
