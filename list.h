#include <stdlib.h>
#define NEWNODE (node*)malloc(sizeof(node))

typedef struct node_
{
    int value;
    struct node_* next;
} node;

node* new_list(int num_nodes);
node* find_value(node* head, int value);

int rem_node(node** head, node* n);
node* add_node(node* head, int value);

int make_array(node* head,int** arr); 
