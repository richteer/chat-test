#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

#define NUM_CON 10

int* new_fd;

int receive_messages(int socket,int sock_num)
{
    int bytes = 0;
    char* name = (char*) calloc(128,sizeof(char));
    bytes = recv(socket,name,128,0);
    char* newline = strchr(name,'\n');
    while (newline != NULL)
    {
        *newline = '\0';
        newline = strchr(name,'\n');
    }
    
    printf("#%i set username as %s\n",sock_num,name);
    
    char* buffer = (char*) calloc(256,sizeof(char));
    if (buffer == NULL)
    {
        printf("Error creating bugger\n");
        return 0;
    }
    while ((bytes = recv(socket,buffer,256,0)) != 0)
    {
        //printf("Received %i bytes from %i\n",bytes,sock_num);
        //printf("Message from #%i: %s",sock_num,buffer);
        send_to_all(name,buffer);
        memset(buffer,0,256);
    }
    printf("%i has disconnected.\n",sock_num);
    close(socket);
    free(name);
    free(buffer);
    return sock_num;
}

int send_to_all(char* sender,char* message)
{
    int i;
    char* buffer = (char*) calloc(strlen(sender)+258,1);
    
    snprintf(buffer,384,"[%s]: %s",sender,message);
    
    printf("Message from [%s]: %s",sender,message);
    for (i = 0; i < NUM_CON; i++)
    {
        if (new_fd[i] != 0)
            send(new_fd[i],buffer,384,0);
    }
    free(buffer);
    printf("Message sent to all successfully\n");
    return 0;
}

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        printf("Usage: server <port>\n");
        return 1;
    }

    struct addrinfo hints, *res;
    int sockfd;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    getaddrinfo(NULL, argv[1], &hints, &res);
    
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (bind(sockfd, res->ai_addr, res->ai_addrlen) == -1)
        perror("Bind error");
    
    
    if (listen(sockfd,10) == -1)
        perror("Listen error");
        
    printf("Waiting for connection...\n");
    
    struct sockaddr_storage* ext_info = (struct sockaddr_storage*) calloc(NUM_CON,sizeof(struct sockaddr_storage));
    socklen_t addr_size = sizeof(struct sockaddr_storage);
    int pid;
    int sock_num,addnum;
    node* head = new_list(NUM_CON);
    new_fd = (int*) calloc(NUM_CON,sizeof(int));
    while(1)
    {
        sock_num = head->value;
        new_fd[sock_num] = accept(sockfd, (struct sockaddr*) &ext_info[sock_num], &addr_size);
        rem_node(&head,find_value(head,sock_num));
        pid = fork();
        if (pid == 0)
        {
            printf("New connection!\n");
            addnum = receive_messages(new_fd[sock_num],sock_num);
            add_node(head,addnum);
            new_fd[addnum] = 0;
            return 0;
        }
    }

    return 0;
}
