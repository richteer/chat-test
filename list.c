#include "list.h"

node* new_list(int num_nodes)
{
    node* head = NEWNODE;
    node* curr = head;
    int i;
    for (i = 0; i < num_nodes-1; i++)
    {
        curr->value = i;
        curr->next = NEWNODE;
        curr = curr->next;
    }
    curr->value = num_nodes-1;
    curr->next = NULL;
    
    return head;
}

node* find_value(node* head, int value)
{
    node* curr = head;
    while (curr != NULL)
    {
        if (curr->value == value)
            return curr;
        
        curr = curr->next;
    }
    return NULL;
}

int rem_node(node** head, node* n)
{
    if ((n == NULL) || ((*head) == NULL))
        return -1;
    if ((*head) == n)
    {
        if ((*head)->next == NULL)
        {
            free(head);
            return 1;
        }
        node* temp = *head;
        *head = ((*head)->next);
        free(temp);
        return 2;
    }
    
    node* curr = *head;
    while ((curr->next != n) && (curr->next != NULL))
    {
        curr = curr->next;
    }
    if (curr->next == NULL)
        return -2;
    curr->next = curr->next->next;
    free(n);
    return 0;
}

node* add_node(node* head, int value)
{
    if (head == NULL)
        return NULL;
        
    node* curr = head;
    while (curr->next != NULL)
    {
        curr = curr->next;
    }
    curr->next = NEWNODE;
    curr->next->value = value;
    curr->next->next = NULL;
    return 0;
}

int make_array(node* head,int** arr)
{
    if (head == NULL)
        return 0;
        
    int count = 0;
    node* curr = head;
    while (curr->next != NULL)
    {
        curr = curr->next;
        count++;
    }
    count++;
    *arr = (int*) malloc(sizeof(int)*count);
    curr = head;
    count = 0;

    while (curr->next != NULL)
    {
        (*arr)[count] = curr->value;
        curr = curr->next;
        count++;
    }
    (*arr)[count] = curr->value;
    
    return count+1;
}
